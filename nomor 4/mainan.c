#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main(int argc, char **argv) {
    pid_t child_id;
    int hour, minute, second;
    char *args[100];
    int i = 0;

    if (strcmp(argv[1], "*") != 0) {
        hour = atoi(argv[1]); //ASCII to int dalam lib stdlib.h, ubah char menjadi int
        if (hour < 0 || hour > 23) {
            fprintf(stderr, "Error: Invalid hour argument.\n");
            exit(1);
        }
    }

    if (strcmp(argv[2], "*") != 0) {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            fprintf(stderr, "Error: Invalid minute argument.\n");
            exit(1);
        }
    }

    if (strcmp(argv[3], "*") != 0) {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            fprintf(stderr, "Error: Invalid second argument.\n");
            exit(1);
        }
    }

    //membentuk command untuk dijalankan
    args[i++] = "/bin/bash";
    args[i++] = "-c";
    args[i++] = argv[4];
    args[i] = NULL;

    char *filename = argv[4];
    chmod(filename, S_IRWXU); //memberi hak akses read dan execute

    if (chmod(filename, S_IRWXU) < 0) {
        perror("Error: Failed to set execute permission.");
        exit(1);
    }

    //fork dan jalankan program di background
    child_id = fork();
    if (child_id < 0) {
        fprintf(stderr, "Error: Failed to fork.\n");
        exit(1);
    } 
    else if (child_id == 0) {
        //child process
        time_t now;
        struct tm *tm_now;
        int delay;

        while (1) {
            //mengambil current time
            now = time(NULL);
            tm_now = localtime(&now);

            if (strcmp(argv[1], "*") == 0) {
                //menggunakan semua nilai jam yang mungkin
                hour = tm_now->tm_hour;
            }

            if (strcmp(argv[2], "*") == 0)  {
                //menggunakan semua nilai menit yang mungkin
                minute = tm_now->tm_min;
            }

            if (strcmp(argv[3], "*") == 0)  {
                //menggunakan semua nilai detik yang mungkin
                second = tm_now->tm_sec;
            }

            //menghitung delay berapa detik sampai jadwal
            delay = (hour - tm_now->tm_hour) * 3600 +
                    (minute - tm_now->tm_min) * 60 +
                    (second - tm_now->tm_sec);

            if (delay < 0) {
                //apabila jadwal sudah lewat, hitung ulang untuk besok
                delay += 24 * 3600;
            }

            //menunggu delay selesai
            sleep(delay);

            //menjalankan program
            if (execvp(args[0], args) < 0) {
                fprintf(stderr, "Error: Failed to execute program.\n");
                exit(1);
            }
        }

        exit(0);
    } 
    else {
        wait(NULL);
    }

    return 0;
}
