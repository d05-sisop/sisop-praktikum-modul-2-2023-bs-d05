#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdbool.h>

#define MAN_UTD_TEAM "Manchester_United"
#define NUM_POSITIONS 4

typedef struct {
    char* name;
    char* position;
    int rating;
} Player;

Player* players;
int num_players;

int main(){
    // Fungsi untuk mengunduh file database
void downloadFile() {
    printf("Mengunduh file database...\n");
    
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork error");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Child process
        char *args[] = {"curl", "-O", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        int status;
        wait(&status);
        if (WEXITSTATUS(status) != 0) {
            printf("Error: failed to download file.\n");
            exit(EXIT_FAILURE);
        }
    }
}

// Fungsi untuk mengekstrak file zip
void extractZip() {
    printf("Mengekstrak file zip...\n");

    pid_t pid = fork();
    if (pid == -1) {
        perror("fork error");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Child process
        char *args[] = {"unzip", "players.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        int status;
        wait(&status);
        if (WEXITSTATUS(status) != 0) {
            printf("Error: failed to extract zip file.\n");
            exit(EXIT_FAILURE);
        }
    }

    // Hapus file zip
    remove("players.zip");
}

// Unduh file
    downloadFile();

 // Ekstrak file zip
    extractZip();

   

// Fungsi untuk menghapus pemain yang bukan dari Manchester United
void filterPlayers() {
    printf("Menghapus pemain yang bukan dari Manchester United...\n");

    DIR *dir;
    struct dirent *entry;

    dir = opendir(".");
    if (dir == NULL) {
        perror("opendir error");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char *filename = entry->d_name;
            char *extension = strrchr(filename, '.');
            if (extension != NULL && strcmp(extension, ".png") == 0) {
                char *tokens[4];
                char *token = strtok(filename, "_");
                int i = 0;
                while (token != NULL) {
                    tokens[i++] = token;
                    token = strtok(NULL, "_");
                }

                if (strcmp(tokens[1], MAN_UTD_TEAM) != 0) {
                    printf("Hapus pemain %s\n", filename);
                    remove(filename);
                }
            }
        }
    }

    closedir(dir);
}

 // Filter pemain
    filterPlayers();

void create_folder(char *folder_name) {
    pid_t pid = fork();
    if (pid == 0) {
        char *args[] = {"mkdir", folder_name, NULL};
        execvp(args[0], args);
    }
    wait(NULL);
}

void move_file(char *file_path, char *folder_name) {
    pid_t pid = fork();
    if (pid == 0) {
        char *args[] = {"mv", file_path, folder_name, NULL};
        execvp(args[0], args);
    }
    wait(NULL);
}

void categorize_players() {
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(MAN_UTD_TEAM)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            char *filename = ent->d_name;
            if (strcmp(filename, ".") != 0 && strcmp(filename, "..") != 0) {
                char *posisi;
                if (strstr(filename, "Kiper") != NULL) {
                    posisi = "Kiper";
                } else if (strstr(filename, "Bek") != NULL) {
                    posisi = "Bek";
                } else if (strstr(filename, "Gelandang") != NULL) {
                    posisi = "Gelandang";
                } else if (strstr(filename, "Penyerang") != NULL) {
                    posisi = "Penyerang";
                }
                char *folder_name = malloc(strlen(posisi) + 1 + strlen(MAN_UTD_TEAM) + 1);
                sprintf(folder_name, "%s/%s", MAN_UTD_TEAM, posisi);
                create_folder(folder_name);
                char *file_path = malloc(strlen(filename) + 1 + strlen(posisi) + 1 + strlen(MAN_UTD_TEAM) + 1);
                sprintf(file_path, "%s/%s/%s", MAN_UTD_TEAM, posisi, filename);
                move_file(file_path, folder_name);
                free(file_path);
                free(folder_name);
            }
        }
        closedir(dir);
    }
}

// Fungsi untuk membandingkan rating dua pemain
int compare(const void* a, const void* b) {
    Player* p1 = (Player*)a;
    Player* p2 = (Player*)b;
    return p2->rating - p1->rating;
}

// Fungsi untuk mengurutkan pemain berdasarkan rating
void sort_players() {
    qsort(players, num_players, sizeof(Player), compare);
}

// Fungsi untuk membaca database pemain dari file
void read_players_database() {
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("players_db.txt", "r");
    if (fp == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    num_players = 0;
    while ((read = getline(&line, &len, fp)) != -1) {
        char *name = strtok(line, ",");
        char *position = strtok(NULL);
    // Kategorisasi pemain
categorize_players();

// Mengurutkan pemain berdasarkan rating
sort_players();

// Inisialisasi variabel tim
char *tim[11];
int num_kiper = 0;
int num_bek = 0;
int num_gelandang = 0;
int num_penyerang = 0;

// Memilih pemain terbaik untuk setiap posisi
for (int i = 0; i < num_players; i++) {
    char *posisi = players[i].posisi;
    if (strcmp(posisi, "Kiper") == 0 && num_kiper < 1) {
        tim[0] = players[i].nama;
        num_kiper++;
    } else if (strcmp(posisi, "Bek") == 0 && num_bek < 4) {
        tim[num_bek + 1] = players[i].nama;
        num_bek++;
    } else if (strcmp(posisi, "Gelandang") == 0 && num_gelandang < 4) {
        tim[num_gelandang + 5] = players[i].nama;
        num_gelandang++;
    } else if (strcmp(posisi, "Penyerang") == 0 && num_penyerang < 2) {
        tim[num_penyerang + 9] = players[i].nama;
        num_penyerang++;
    }
    if (num_kiper == 1 && num_bek == 4 && num_gelandang == 4 && num_penyerang == 2) {
        break;
    }
}
    }

// Menampilkan tim terbaik
printf("Tim Kesebelasan Terbaik Manchester United:\n");
printf("Kiper: %s\n", tim[0]);
for (int i = 1; i < 5; i++) {
    printf("Bek %d: %s\n", i, tim[i]);
}
for (int i = 5; i < 9; i++) {
    printf("Gelandang %d: %s\n", i - 4, tim[i]);
}
for (int i = 9; i < 11; i++) {
    printf("Penyerang %d: %s\n", i - 8, tim[i]);
}

// Memfilter pemain yang bukan dari Manchester United
filterPlayers();

// Mencari kesebelasan terbaik
find_best_team();

return 0;
}
}

