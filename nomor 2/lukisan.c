#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <signal.h>
#include <sys/prctl.h>
#include <stdbool.h>

void getTime(char *currentTime)
{
	time_t t;
	t = time(NULL);
	struct tm* localTime = localtime(&t);
	strftime(currentTime, 20, "%Y-%m-%d_%H:%M:%S", localTime);
}


void createFolder(char *folderName)
{
	pid_t child;
	child = fork();
	if(child == 0){
		char *argv[3] = {"mkdir", folderName, NULL};
		execv("/bin/mkdir", argv);
	}
	while(wait(NULL)!=child);
}


void downloadImage(char *folderName)
{
	pid_t child;
	char name[20];
	char path[50];
	char downloadLink[60];

	for(int i=0; i<2; i++){
		child = fork();

		if(child == 0){
			time_t t = time(NULL);
			struct tm* currTime = localtime(&t);

			strftime(name, 20, "%Y-%m-%d_%H:%M:%S", currTime);
			sprintf(path, "%s/%s", folderName, name);
			sprintf(downloadLink, "https://picsum.photos/%ld", (t%1000)+50);

			char *argv[6] = {"wget","-q", "-O", path, downloadLink, NULL};
			execv("/bin/wget", argv);
		}
		sleep(5);
	}
	while(wait(NULL)>0);
}

void backupFolder(char *folderName)
{
	char name[25];

	pid_t child;
	child = fork();

	sprintf(name, "%s.zip", folderName);

	if(child == 0){
		char *argv[5] = {"zip", "-r", name, folderName, NULL};
		execv("/bin/zip", argv);
	}
	while(wait(NULL)!=child);
}

void rmFolder(char *folderName)
{
	char *argv[4] = {"rm", "-r", folderName, NULL};
	execv("/bin/rm", argv);	
}

void programKiller(char *argv[], pid_t pid, pid_t sid)
{

	FILE *fileptr = fopen("killer.c", "W");

	//killer.c
	char input[1000] = " "
	"#include <unistd.h>\n"
	"#include <wait.h>\n"
	"int main() {\n"
	"pid_t child = fork();\n"
	"if(child == 0) {\n"
		"%s"
		"execv(\"%s\", argv;\n"
		"}\n"
		"while(wait(NULL)>0);\n"
		"char *argv[] = {\"rm\", \"killer\", NULL};\n"
		"execv(\"/bin/rm\", argv);\n"
	"}\n";

	//MODE_A
	char command[1000];
	if(strcmp(argv[1], "-a") == 0){
		sprintf(command, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
		fprintf(fileptr, input, command, "/usr/bin/pkill");
	}

	//MODE_B
	if(strcmp(argv[1], "-b") == 0){
		sprintf(command, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
		fprintf(fileptr, input, command, "/bin/kill");
	}

	fclose(fileptr);

	//Compile
	pid = fork();
	if(pid == 0){
		char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
		execv("usr/bin/gcc", command);
	}
	while(wait(NULL) != pid);

	//Remove
	pid = fork();
	if (pid == 0){
		char *argv[] = {"rm", "killer.c", NULL};
		execv("/bin/rm", argv);
	}
	while(wait(NULL) != pid);
}

int main(char *argv[])
{
	//Membuat Daemon
	pid_t pid, sid;
	pid = fork();

	if(pid<0){
		exit(EXIT_FAILURE);
	}

	if(pid>0){
		exit(EXIT_SUCCESS);
	}

	umask(0);
	sid = setsid();

	if(sid<0){
		exit(EXIT_FAILURE);
	}

	programKiller(argv, pid, sid);
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	while(1)
	{
		//Input Time
		char currentTime[20];
		getTime(currentTime);

		//Membuat Folder
		createFolder(currentTime);

		//Download, Zip, Remove Gambar
		pid_t child;
		child = fork();
		if(child == 0){
			downloadImage(currentTime);
			backupFolder(currentTime);
			rmFolder(currentTime);
		}
		sleep(30);
	}
}





//Reference
//codescracker.com/c/program/c-program-print-date.htm
