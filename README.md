# sisop-praktikum-modul-2-2023-BS-D05

## Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

1. Grape-kun harus mendownload dari link drive yang nantinya akan disimpan dalam folder zip.
```
void download(char *link, char *fname) {
    pid_t child_id;
    child_id = fork();    
    if (child_id == 0) {
        execl("/usr/bin/wget", "/usr/bin/wget", "--no-check-certificate", link, "-O", fname, NULL);
        exit(0);
    } 
    else {
        int status;
        waitpid(child_id, &status, 0);
    }
}
```
Fungsi akan membuat child baru dan menggunakan perintah excl untuk menjalankan bin/wget. 

2. Selanjutnya, harus melakukan unzip pada file tersebut. Fungsi zip akan dijalankan pada proses anak baru yang akan dibuat terlebih dahulu.
```
void unzip() {
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        char *argv[] = {"unzip", "hewan.zip", "-d", "/home/farah/modul2/hewan/", NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    while (wait(&status) > 0);
}
```
Setelah file diunzip, akan diletakkan pada direktori `/home/farah/modul2/hewan/`.

3. Setelah itu, file-file dalam direktori tersebut akan dipisah dan dikategorikan berdasarkan hewan air, hewan darat, dan hewan amphibi. 

Sebelum itu harus dibuat folder untuk mengkategorikan file-file tersebut.
```
void create_folder(char *kategori) {
    char Dir[100] = "/home/farah/modul2/hewan/";
    strcat(Dir, kategori);

    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", Dir, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}
```
Setelah dibuat folder berdasarkan kategorinya, file-file tersebut dibaca string nama file berdasarkan kategorinya.
```
void managePos() {
    struct dirent *ep;
    DIR *dp = opendir("/home/farah/modul2/hewan");
    if (dp != NULL) {
        while ((ep = readdir(dp))) {
            if (strstr(ep->d_name, ".jpg")) {

                if (strstr(ep->d_name, "darat") != NULL){
                        movePos("/HewanDarat", ep->d_name);
                }
                else if (strstr(ep->d_name, "amphibi") != NULL){
                        movePos("/HewanAmphibi", ep->d_name);
                }
                else if (strstr(ep->d_name, "air") != NULL){
                        movePos("/HewanAir", ep->d_name);
                }
                else{
                        // File yang tidak sesuai kategori dipindahkan ke folder "Lainnya"
                        movePos("/Lainnya", ep->d_name);
                }
            }
        }
    }

    closedir(dp);
}
```
Setelah itu akan dijalankan fungsi movePos yang akan memindahkan file-file tersebut berdasarkan string namanya.
```
void movePos(const char *kategori, char *filename) {
    char file_path[200];
    strcpy(file_path, "/home/farah/modul2/hewan/");
    strcat(file_path, filename);

    char dest_path[200];
    strcpy(dest_path, "/home/farah/modul2/hewan");
    strcat(dest_path, kategori);
    strcat(dest_path, "/");
    strcat(dest_path, filename);

    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"mv", file_path, dest_path, NULL};
        execv("/bin/mv", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}
```
4. Grape-kun juga harus melukan shift secara acak. Fungsi akan men-generate random file dan akan dicetak di terminal.
```
void print_random_file() {
    DIR *dp = opendir("/home/farah/modul2/hewan");
    struct dirent *ep;

    if (dp != NULL) {
        int num_files = 0;
        while ((ep = readdir(dp)) != NULL) {
            if (ep->d_type == DT_REG) {
                num_files++;
            }
        }

        if (num_files == 0) {
            printf("No files found in directory.\n");
            closedir(dp);
            return;
        }

        srand(time(NULL));
        int random_num = rand() % num_files;

        rewinddir(dp);
        int count = 0;
        while ((ep = readdir(dp)) != NULL) {
            if (ep->d_type == DT_REG) {
                if (count == random_num) {
                    char* filename = ep->d_name;
                    printf("\nShift saat ini adalah: %s\n", filename);
                    break;
                }
                count++;
            }
        }
    }
    closedir(dp);
}
```
Output hasilnya akan seperti di bawah ini:
[gambar 1](Screenshots/hasil_binatang.c.jpg)

## Soal 2
------lukisan.c

### Penjelasan
- Buat sebuah folder khusus yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ 
- Tiap gambar di download setiap 5 detik dan berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. 
- Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
- Folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
- Generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. 
- Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. - Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### Penyelesaian
1. Fungsi getTime  
Merupakan fungsi untuk return time saat ini.
```
void getTime(char *currentTime)
{
	time_t t;
	t = time(NULL);
	struct tm* localTime = localtime(&t);
	strftime(currentTime, 20, "%Y-%m-%d_%H:%M:%S", localTime);
}
```
- Mengambil waktu berdasarkan localtime sistem menggunakan tipe data `time_t` dari library `<time_h>.
- Set format nama sesuai soal menggunakan `strftime` lalu memasukkan ke pointer parameter yang diterima fungsi.

2. Fungsi createFolder  
Merupakan fungsi untuk membuat folder.
```
void createFolder(char *folderName)
{
	pid_t child;
	child = fork();
	if(child == 0){
		char *argv[3] = {"mkdir", folderName, NULL};
		execv("/bin/mkdir", argv);
	}
	while(wait(NULL)!=child);
}
```
- Mempunyai parameter nama folder yang akan dibuat.
- Melakukan fork, pada child process akan menjalankan `execv` sesuai argumen dari char `argv`.
- Argumen dalam `argv` yaitu mkdir dengan nama seusai parameter.

3. Fungsi downloadImage  
Merupakan fungsi untuk mendownload gambar dan menyimpan sesuai permintaan soal.
```
void downloadImage(char *folderName)
{
	pid_t child;
	char name[20];
	char path[50];
	char downloadLink[60];

	for(int i=0; i<2; i++){
		child = fork();

		if(child == 0){
			time_t t = time(NULL);
			struct tm* currTime = localtime(&t);

			strftime(name, 20, "%Y-%m-%d_%H:%M:%S", currTime);
			sprintf(path, "%s/%s", folderName, name);
			sprintf(downloadLink, "https://picsum.photos/%ld", (t%1000)+50);

			char *argv[6] = {"wget","-q", "-O", path, downloadLink, NULL};
			execv("/bin/wget", argv);
		}
		sleep(5);
	}
	while(wait(NULL)>0);
}
```
- Membuat beberapa array dengan tipe data char yaitu `name` untuk menyimpan nama gambar, `path` untuk menyimpan jalur gambar, dan `downloadLink` untuk menyimpan link mendownload gambar.
- Melakukan perulangan sejumlah 15 kali sesuai permintaan soal untuk mendownload 15 gambar.
- Melakukan fork, pada child process akan melakukan perintah.
- Set nama gambar sesuai format soal menggunakan `strftime` lalu memasukkan kedalam `name`.
- Set path menyimpan gambar yang didownload menggunakan `sprintf` lalu memasukkan kedalam `path`.
- Set link download dari picsum dengan memberi ukuran sesuai permintaan soal pada akhir link menggunakan `sprintf` lalu memasukkan kedalam `downloadLink`.
- Menjalankan `execv` dengan argumen `wget` untuk mendownload gambar.
- `sleep(5)` untuk mendownload gambar setiap 5 detik sekali.


4. Fungsi backupFolder  
Merupakan fungsi untuk melakukan zip pada folder.
```
void backupFolder(char *folderName)
{
	char name[25];

	pid_t child;
	child = fork();

	sprintf(name, "%s.zip", folderName);

	if(child == 0){
		char *argv[5] = {"zip", "-r", name, folderName, NULL};
		execv("/bin/zip", argv);
	}
	while(wait(NULL)!=child);
}
```
- Pointer yang dimasukkan kedalam parameter akan ditambahkan .zip diakhir menggunakan `sprintf`.
- Melakukan fork, child process akan menjalankan `execv` dengan perintah `zip -r`.

5. Fungsi rmFolder  
Merupakan fungsi untuk delete(remove) suatu folder.
```
void rmFolder(char *folderName)
{
	char *argv[4] = {"rm", "-r", folderName, NULL};
	execv("/bin/rm", argv);	
}
```
- `execv` akan menjalankan argumen `rm -r` yang akan meremove seuatu directory secara keseluruhan.
- Directory/folder yang diremove sesuai input parameter.

6. Fungsi programKiller  
Merupakan fungsi untuk menterminasi semua operasi program.
```
void programKiller(char *argv[], pid_t pid, pid_t sid)
{

	FILE *fileptr = fopen("killer.c", "W");

	//killer.c
	char input[1000] = " "
	"#include <unistd.h>\n"
	"#include <wait.h>\n"
	"int main() {\n"
	"pid_t child = fork();\n"
	"if(child == 0) {\n"
		"%s"
		"execv(\"%s\", argv;\n"
		"}\n"
		"while(wait(NULL)>0);\n"
		"char *argv[] = {\"rm\", \"killer\", NULL};\n"
		"execv(\"/bin/rm\", argv);\n"
	"}\n";

	//MODE_A
	char command[1000];
	if(strcmp(argv[1], "-a") == 0){
		sprintf(command, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
		fprintf(fileptr, input, command, "/usr/bin/pkill");
	}

	//MODE_B
	if(strcmp(argv[1], "-b") == 0){
		sprintf(command, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
		fprintf(fileptr, input, command, "/bin/kill");
	}

	fclose(fileptr);

	//Compile
	pid = fork();
	if(pid == 0){
		char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
		execv("usr/bin/gcc", command);
	}
	while(wait(NULL) != pid);

	//Remove
	pid = fork();
	if (pid == 0){
		char *argv[] = {"rm", "killer.c", NULL};
		execv("/bin/rm", argv);
	}
	while(wait(NULL) != pid);
}
```
- Membuat sebuah file bernama `killer.c` yang berisi perintah terminate program.
- Parameter berisi argumen -a atau -b untuk menentukan mode yang akan dijalankan pada program.
- Pada mode A, `killer.c` akan melakukan `pkill` sid sehingga akan terminate session yang sedang berjalan.
- Pada mode B, `killer.c` akan melakukan `kill` pid saja sehingga child process yang masih berjalan akan menunggu untuk selesai terlebih dahulu.
- `killer.c` lalu dicompile menggunakan execv dengan perintah compile `gcc killer.c -o killer`.
- `killer.c` lalu akan diremove setelah tercompile, remove menggunakan execv dengan argumen `rm killer.c`.


7. Fungsi main
```
int main(char *argv[])
{
	//Membuat Daemon
	pid_t pid, sid;
	pid = fork();

	if(pid<0){
		exit(EXIT_FAILURE);
	}

	if(pid>0){
		exit(EXIT_SUCCESS);
	}

	umask(0);
	sid = setsid();

	if(sid<0){
		exit(EXIT_FAILURE);
	}

	programKiller(argv, pid, sid);
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	while(1)
	{
		//Input Time
		char currentTime[20];
		getTime(currentTime);

		//Membuat Folder
		createFolder(currentTime);

		//Download, Zip, Remove Gambar
		pid_t child;
		child = fork();
		if(child == 0){
			downloadImage(currentTime);
			backupFolder(currentTime);
			rmFolder(currentTime);
		}
		sleep(30);
	}
}
```
- Pada main akan membuat program berjalan secara daemon.
- Membuat daemon sesuai template daemon.
- Akan memanggil fungsi `programKiller` untuk membuat killer.c dari program.
- Pada program daemon, pertama akan membuat sebuah variable `currentTime` yang akan mengambil waktu saat ini dengan memanggil fungsi `getTime`.
- Setelah mendapatkan waktu, memanggil `createFolder` untuk membuat folder terlebih dahulu.
- Program akan melakukan fork.
- Child process akan memanggil `downloadImage` terlebih dahulu untuk mendownload gambar.
- Lalu memanggil `backupFolder` untuk melakukan zip terhadap folder.
- dan terakhir memanggil `rmFolder` untuk meremove folder yang telah dizip.

## Soal 3
-----filter.c



-  Proses Download file, exstract zip, dan hapus zip

`#define MAN_UTD_TEAM "Manchester_United"
#define NUM_POSITIONS 4

typedef struct {
    char* name;
    char* position;
    int rating;
} Player;

Player* players;
int num_players;

int main(){
    // Fungsi untuk mengunduh file database
void downloadFile() {
    printf("Mengunduh file database...\n");
    
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork error");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Child process
        char *args[] = {"curl", "-O", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        int status;
        wait(&status);
        if (WEXITSTATUS(status) != 0) {
            printf("Error: failed to download file.\n");
            exit(EXIT_FAILURE);
        }
    }
}`

`// Fungsi untuk mengekstrak file zip
void extractZip() {
    printf("Mengekstrak file zip...\n");

    pid_t pid = fork();
    if (pid == -1) {
        perror("fork error");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Child process
        char *args[] = {"unzip", "players.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        int status;
        wait(&status);
        if (WEXITSTATUS(status) != 0) {
            printf("Error: failed to extract zip file.\n");
            exit(EXIT_FAILURE);
        }
    }

    // Hapus file zip
    remove("players.zip");
}

// Unduh file
    downloadFile();

 // Ekstrak file zip
    extractZip();`

## Penjelasan
#define MAN_UTD_TEAM "Manchester_United"
Mendefinisikan konstanta string MAN_UTD_TEAM dengan nilai "Manchester_United". Konstanta ini digunakan untuk mengacu pada nama folder yang berisi daftar pemain Manchester United.

#define NUM_POSITIONS 4
Mendefinisikan konstanta integer NUM_POSITIONS dengan nilai 4. Konstanta ini digunakan untuk menentukan jumlah posisi yang akan dikategorikan pada pemain, yaitu kiper, bek, gelandang, dan penyerang.

int main()
Fungsi utama yang menjalankan program. Pada fungsi ini, terdapat dua fungsi yang dipanggil yaitu downloadFile() dan extractZip(). Kedua fungsi ini bertanggung jawab untuk mengunduh dan mengekstrak file zip berisi daftar pemain Manchester United.

a. void downloadFile()
Fungsi untuk mengunduh file database dari Google Drive menggunakan perintah curl. Fungsi ini menggunakan fungsi fork() untuk memisahkan proses parent dan child. Jika fungsi fork() mengembalikan nilai -1, maka proses fork() gagal dan fungsi perror() akan menampilkan pesan kesalahan. Jika nilai yang dikembalikan adalah 0, maka proses yang dijalankan adalah child process. Child process akan menjalankan perintah curl untuk mengunduh file database. Jika curl gagal, maka child process akan keluar dengan status EXIT_FAILURE. Jika nilai yang dikembalikan oleh fungsi fork() adalah nilai selain -1 atau 0, maka proses yang dijalankan adalah parent process. Parent process akan menunggu hingga child process selesai dieksekusi. Jika child process mengembalikan status keluar yang bukan nol, maka parent process akan menampilkan pesan kesalahan.

b. void extractZip()
Fungsi untuk mengekstrak file zip berisi daftar pemain Manchester United menggunakan perintah unzip. Fungsi ini juga menggunakan fungsi fork() untuk memisahkan proses parent dan child. Jika fungsi fork() mengembalikan nilai -1, maka proses fork() gagal dan fungsi perror() akan menampilkan pesan kesalahan. Jika nilai yang dikembalikan adalah 0, maka proses yang dijalankan adalah child process. Child process akan menjalankan perintah unzip untuk mengekstrak file zip. Jika unzip gagal, maka child process akan keluar dengan status EXIT_FAILURE. Jika nilai yang dikembalikan oleh fungsi fork() adalah nilai selain -1 atau 0, maka proses yang dijalankan adalah parent process. Parent process akan menunggu hingga child process selesai dieksekusi. Jika child process mengembalikan status keluar yang bukan nol, maka parent process akan menampilkan pesan kesalahan.


-  Proses Penghapusan Pemain yang bukan dari "Manchaster United"

// Fungsi untuk menghapus pemain yang bukan dari Manchester United
``
void filterPlayers() {
    printf("Menghapus pemain yang bukan dari Manchester United...\n");

    DIR *dir;
    struct dirent *entry;

    dir = opendir(".");
    if (dir == NULL) {
        perror("opendir error");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char *filename = entry->d_name;
            char *extension = strrchr(filename, '.');
            if (extension != NULL && strcmp(extension, ".png") == 0) {
                char *tokens[4];
                char *token = strtok(filename, "_");
                int i = 0;
                while (token != NULL) {
                    tokens[i++] = token;
                    token = strtok(NULL, "_");
                }

                if (strcmp(tokens[1], MAN_UTD_TEAM) != 0) {
                    printf("Hapus pemain %s\n", filename);
                    remove(filename);
                }
            }
        }
    }

    closedir(dir);
}`

## Penjelasan
Fungsi filterPlayers() digunakan untuk menghapus pemain yang bukan dari tim Manchester United. Fungsi ini melakukan langkah-langkah berikut:

Membuka direktori tempat file-file pemain disimpan dengan menggunakan opendir(".").
Melakukan perulangan menggunakan readdir(dir) untuk membaca setiap file di dalam direktori tersebut.
Mengecek apakah file tersebut merupakan file regular (bukan direktori) dengan menggunakan entry->d_type == DT_REG.
Memeriksa apakah ekstensi file tersebut adalah ".png" dengan menggunakan strcmp(extension, ".png") == 0.
Jika file tersebut adalah file pemain (berdasarkan ekstensi dan format nama file), maka fungsi ini memeriksa apakah pemain tersebut berasal dari Manchester United dengan memisahkan token-token dalam nama file tersebut menggunakan _ sebagai pemisah, lalu memeriksa apakah token kedua (yang mengandung nama tim) sama dengan MAN_UTD_TEAM menggunakan strcmp(tokens[1], MAN_UTD_TEAM) != 0.
Jika pemain tersebut bukan berasal dari Manchester United, maka fungsi ini akan menghapus file tersebut dengan menggunakan remove(filename).

Maka fungsi ini bertujuan untuk memastikan bahwa hanya pemain dari Manchester United yang tersisa dalam database yang digunakan dalam program ini.


- Pembuataan 4 folder, dan perpindahan pemain berdasarkan posisinya
`void create_folder(char *folder_name) {
    pid_t pid = fork();
    if (pid == 0) {
        char *args[] = {"mkdir", folder_name, NULL};
        execvp(args[0], args);
    }
    wait(NULL);
}

void move_file(char *file_path, char *folder_name) {
    pid_t pid = fork();
    if (pid == 0) {
        char *args[] = {"mv", file_path, folder_name, NULL};
        execvp(args[0], args);
    }
    wait(NULL);
}

void categorize_players() {
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(MAN_UTD_TEAM)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            char *filename = ent->d_name;
            if (strcmp(filename, ".") != 0 && strcmp(filename, "..") != 0) {
                char *posisi;
                if (strstr(filename, "Kiper") != NULL) {
                    posisi = "Kiper";
                } else if (strstr(filename, "Bek") != NULL) {
                    posisi = "Bek";
                } else if (strstr(filename, "Gelandang") != NULL) {
                    posisi = "Gelandang";
                } else if (strstr(filename, "Penyerang") != NULL) {
                    posisi = "Penyerang";
                }
                char *folder_name = malloc(strlen(posisi) + 1 + strlen(MAN_UTD_TEAM) + 1);
                sprintf(folder_name, "%s/%s", MAN_UTD_TEAM, posisi);
                create_folder(folder_name);
                char *file_path = malloc(strlen(filename) + 1 + strlen(posisi) + 1 + strlen(MAN_UTD_TEAM) + 1);
                sprintf(file_path, "%s/%s/%s", MAN_UTD_TEAM, posisi, filename);
                move_file(file_path, folder_name);
                free(file_path);
                free(folder_name);
            }
        }
        closedir(dir);
    }
}`

## Penjelasan
Fungsi filterPlayers() memfilter gambar pemain dan menghapus yang tidak termasuk dalam tim Manchester United. Ia membaca file dalam direktori saat ini, memeriksa apakah setiap file memiliki ekstensi ".png", dan jika ya, mengekstrak nama tim dari nama file. Jika nama tim tidak cocok dengan "Manchester_United", maka file tersebut dihapus.

Fungsi create_folder() membuat folder baru dengan folder_name yang diberikan. Ia melakukan fork pada proses baru dan kemudian memanggil perintah mkdir dengan folder_name sebagai argumen.

Fungsi move_file() memindahkan file yang ditentukan oleh file_path ke folder yang ditentukan oleh folder_name. Ia melakukan fork pada proses baru dan kemudian memanggil perintah mv dengan file_path dan folder_name sebagai argumen.

Fungsi categorize_players() membuka direktori yang bernama "Manchester_United", membaca file dalam direktori tersebut, dan mengkategorikan gambar pemain berdasarkan posisinya (Kiper, Bek, Gelandang, atau Penyerang). Untuk setiap file, ia menentukan posisi pemain berdasarkan substring dari nama file, membuat folder baru dengan nama posisi di dalam direktori "Manchester_United", dan kemudian memindahkan file ke folder posisi yang sesuai dengan menggunakan fungsi create_folder() dan move_file().

- Pembuatan tim kesebelasan terbaik berdasarkan rating
`/ Fungsi untuk membandingkan rating dua pemain
int compare(const void* a, const void* b) {
    Player* p1 = (Player*)a;
    Player* p2 = (Player*)b;
    return p2->rating - p1->rating;
}

// Fungsi untuk mengurutkan pemain berdasarkan rating
void sort_players() {
    qsort(players, num_players, sizeof(Player), compare);
}

// Fungsi untuk membaca database pemain dari file
void read_players_database() {
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("players_db.txt", "r");
    if (fp == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    num_players = 0;
    while ((read = getline(&line, &len, fp)) != -1) {
        char *name = strtok(line, ",");
        char *position = strtok(NULL);
    // Kategorisasi pemain
categorize_players();

// Mengurutkan pemain berdasarkan rating
sort_players();

// Inisialisasi variabel tim
char *tim[11];
int num_kiper = 0;
int num_bek = 0;
int num_gelandang = 0;
int num_penyerang = 0;
`
`// Memilih pemain terbaik untuk setiap posisi
for (int i = 0; i < num_players; i++) {
    char *posisi = players[i].posisi;
    if (strcmp(posisi, "Kiper") == 0 && num_kiper < 1) {
        tim[0] = players[i].nama;
        num_kiper++;
    } else if (strcmp(posisi, "Bek") == 0 && num_bek < 4) {
        tim[num_bek + 1] = players[i].nama;
        num_bek++;
    } else if (strcmp(posisi, "Gelandang") == 0 && num_gelandang < 4) {
        tim[num_gelandang + 5] = players[i].nama;
        num_gelandang++;
    } else if (strcmp(posisi, "Penyerang") == 0 && num_penyerang < 2) {
        tim[num_penyerang + 9] = players[i].nama;
        num_penyerang++;
    }
    if (num_kiper == 1 && num_bek == 4 && num_gelandang == 4 && num_penyerang == 2) {
        break;
    }
}
    }

// Menampilkan tim terbaik
printf("Tim Kesebelasan Terbaik Manchester United:\n");
printf("Kiper: %s\n", tim[0]);
for (int i = 1; i < 5; i++) {
    printf("Bek %d: %s\n", i, tim[i]);
}
for (int i = 5; i < 9; i++) {
    printf("Gelandang %d: %s\n", i - 4, tim[i]);
}
for (int i = 9; i < 11; i++) {
    printf("Penyerang %d: %s\n", i - 8, tim[i]);
}

// Memfilter pemain yang bukan dari Manchester United
filterPlayers();

// Mencari kesebelasan terbaik
find_best_team();

return 0;
}
}`

## Penjelasan 
Fungsi "compare()" untuk membandingkan rating dua pemain dengan parameter "a" dan "b" yang berisi pointer ke struct Player. Fungsi ini akan mengembalikan nilai selisih rating dari pemain kedua dikurangi dengan rating pemain pertama.

Fungsi "sort_players()" untuk mengurutkan pemain berdasarkan rating dengan memanggil fungsi "qsort()" dan mengirimkan array "players" beserta jumlah pemain dan ukuran data tiap elemen.

Fungsi "read_players_database()" untuk membaca database pemain dari file "players_db.txt". Pada fungsi ini, dilakukan pembukaan file dan membaca data pemain satu per satu dari file tersebut. Setiap data pemain akan dikategorikan posisinya menggunakan fungsi "categorize_players()", kemudian diurutkan menggunakan fungsi "sort_players()".

Fungsi "find_best_team()" untuk memilih pemain terbaik untuk setiap posisi dalam kesebelasan Manchester United. Pemilihan pemain terbaik dilakukan dengan memilih pemain dengan rating tertinggi pada posisi yang dibutuhkan dan memasukkannya ke dalam array "tim". Pemilihan posisi dan penambahan pemain ke dalam array "tim" dilakukan dengan menggunakan statement "if-else" dan "for" pada program.

Fungsi "filterPlayers()" untuk memfilter pemain yang bukan dari Manchester United. Pemfilteran dilakukan dengan memeriksa ekstensi file gambar pemain dan menghapus file yang ekstensinya bukan ".png" atau nama timnya bukan "Manchester_United".

Setelah proses pemilihan kesebelasan selesai, program akan menampilkan kesebelasan terbaik Manchester United dan melakukan pemfilteran pemain yang bukan dari Manchester United dengan memanggil fungsi "filterPlayers()". Selanjutnya, program akan mencari kesebelasan terbaik dengan memanggil fungsi "find_best_team()". Setelah itu, program akan mengembalikan nilai 0 sebagai penanda program berhasil dijalankan.

## Soal 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Pada dasarnya ini adalah konsep cronjob dalam bahasa C. Program cronjob akan menjalankan suatu kode program sesuai dengan waktu yang ditentukan. Waktu bisa berupa jam, menit, dan detik, adapun symbol (*) akan diartikan sebagai semua angka possible.

1. Argumen pertama akan membaca jam. `atoi(argv[1])` akan mengubah argumen 1 yang masih berbentuk char menjadi int.
```
if (strcmp(argv[1], "*") != 0) {
        hour = atoi(argv[1]); 
        if (hour < 0 || hour > 23) {
            fprintf(stderr, "Error: Invalid hour argument.\n");
            exit(1);
        }
}
```
2. Argumen kedua akan membaca menit. `atoi(argv[1])` akan mengubah argumen 1 yang masih berbentuk char menjadi int.
```
if (strcmp(argv[2], "*") != 0) {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            fprintf(stderr, "Error: Invalid minute argument.\n");
            exit(1);
        }
}
```
3. Argumen ketiga akan membaca detik. `atoi(argv[1])` akan mengubah argumen 1 yang masih berbentuk char menjadi int.
```
if (strcmp(argv[3], "*") != 0) {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            fprintf(stderr, "Error: Invalid second argument.\n");
            exit(1);
        }
}
```
4. Membentuk command untuk dijalankan, hak akses akan diberikan melalui `S_IRWXU`
```
args[i++] = "/bin/bash";
args[i++] = "-c";
args[i++] = argv[4];
args[i] = NULL;

char *filename = argv[4];
chmod(filename, S_IRWXU); //memberi hak akses read dan execute

if (chmod(filename, S_IRWXU) < 0) {
    perror("Error: Failed to set execute permission.");
    exit(1);
}
```
5. Karena (*) adalah semua angka possible, maka setiap ada symbol tersebut, akan dimasukkan sebagai jadwal dari cronjob.
```
if (strcmp(argv[1], "*") == 0) {
        //menggunakan semua nilai jam yang mungkin
        hour = tm_now->tm_hour;
}

if (strcmp(argv[2], "*") == 0)  {
    //menggunakan semua nilai menit yang mungkin
    minute = tm_now->tm_min;
}

if (strcmp(argv[3], "*") == 0)  {
    //menggunakan semua nilai detik yang mungkin
    second = tm_now->tm_sec;
}
```
Karena dimasukkan sebagai waktu terkini menjadi waktu yang akan menjalankan program, maka tidak akan ada delay yang terjadi.

6. Sebaliknya jika ada angka, maka akan dihitung berapa detik menuju jadwal dari cronjob tersebut.
```
delay = (hour - tm_now->tm_hour) * 3600 +
        (minute - tm_now->tm_min) * 60 +
        (second - tm_now->tm_sec);
```
Jika jadwal sudah terlewat, akan dilakukan besoknya.
```
if (delay < 0) {
    delay += 24 * 3600;
}
sleep(delay);
```

